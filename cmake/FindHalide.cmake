find_path(Halide_INCLUDE_DIR Halide.h)
find_library(Halide_LIBRARY Halide)
mark_as_advanced(Halide_INCLUDE_DIR Halide_LIBRARY Halide_DIR)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Halide
    REQUIRED_VARS Halide_INCLUDE_DIR Halide_LIBRARY
)

if(Halide_FOUND AND NOT TARGET Halide::Halide)
    add_library(Halide::Halide UNKNOWN IMPORTED)
    set_target_properties(Halide::Halide PROPERTIES
        IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
        IMPORTED_LOCATION "${Halide_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES "${Halide_INCLUDE_DIR}"
    )
endif()
