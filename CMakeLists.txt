cmake_minimum_required(VERSION 3.2 FATAL_ERROR)

add_executable(halide-generators
    src/halide-generators.cpp
    src/gpu-match.cpp
)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

find_package(Halide REQUIRED)

target_link_libraries(halide-generators
    PRIVATE
        Halide::Halide
)

target_compile_features(halide-generators
    PRIVATE
        cxx_auto_type
        cxx_range_for
        cxx_variadic_templates
        cxx_constexpr
        cxx_decltype_auto
)

target_compile_options(halide-generators PRIVATE -fno-rtti)

add_custom_target(gen-html
    COMMAND halide-generators -g GpuMatch -o ${CMAKE_CURRENT_BINARY_DIR} -e html target=cuda
    DEPENDS halide-generators
)
