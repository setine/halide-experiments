#pragma once

#include <Halide.h>

class GpuMatch : public Halide::Generator<GpuMatch> {
public:
    Halide::ImageParam input1{Float(32), 2, "input1"};
    Halide::ImageParam input2{Float(32), 2, "input2"};
    Halide::Var x{"x"}, y{"y"};

    Halide::Func build();
};
