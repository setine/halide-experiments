#include "gpu-match.h"

using namespace Halide;

RegisterGenerator<GpuMatch>
    gpuMatchGenerator{"GpuMatch"};

Func GpuMatch::build()
{
    Var c("c"), i("i");

    Func diff("diff"), diffSq("diffSq"), dotp("dotp"), out("out"),
         inp1("inp1"), inp2("inp2"), minVal("minVal");

    inp1(c,x) = input1(c,x);
    inp2(c,y) = input2(c,y);

    diff(x,y,c) = inp1(c, x) - inp2(c, y);
    diffSq(x,y,c) = diff(x,y,c) * diff(x,y,c);

    RDom rc(0,128);
    dotp(x, y) = 0.f;
    dotp(x, y) += diffSq(x, y, rc);

    // Argmin, see https://github.com/halide/Halide/blob/master/test/correctness/rfactor.cpp#L804
    RDom ry(0, input2.height(), "ry");
    minVal(x) = {std::numeric_limits<float>::max(), -1};
    minVal(x) = {
        min(minVal(x)[0], dotp(x, ry)),
        select(minVal(x)[0] < dotp(x, ry)
              ,minVal(x)[1]
              ,ry)
    };

    out(x) = minVal(x)[1];

    // Schedule
    RVar ryo("ryo"), ryi("ryi");
    Var yy("yy");
    Func intermediate("inter");

    dotp.compute_root();

    minVal.update(0).split(ry, ryo, ryi, 16);
    intermediate = minVal.update().rfactor(ryo, yy);
    //intermediate.compute_root();

    //Var xi;

    //out.tile(x,xi,16);

    return out;
}
